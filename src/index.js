const express = require('express');
const app = express();
const path = require('path');

//SETTING
app.set('port', 3001);
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//middleware

//routes
app.use(require('./routes/index'));

//static public
app.use(express.static(path.join(__dirname, 'public')));

//listening on server
app.listen(app.get('port'), () => {
  console.log('server on port', app.get('port'));
});
